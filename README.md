# conda_fix.py

## Passwordless authentication to online Git repositories in Anaconda on Windows

To keep things simple for you, I have written a Python program which will
perform all of the necessary steps to upgrade your Anaconda command-line
environment for this to work.  You will need to use git to get a copy of my
program. After my program upgrades your Anaconda enviroment it will pop open a
notepad window containing your own SSH key. You will need to log in to your
Bitbucket account and submit the SSH key into your account's settings page
*Note that this is a different settings page than the per-repository settings
page*; be sure that you are in the right place when you set this up!

1. Open the Anaconda Command Line

2. Download this code from Bitbucket with git
   `git clone https://bitbucket.org/erikfalor/conda_fix.git`

3. Move into the new conda_fix directory created by the previous command:
   `cd conda_fix`

4. Run the conda_fix.py Python program:
   `python conda_fix.py`

5. When Notepad opens, follow the instructions printed at the bottom of the
   command window to install your SSH key on Bitbucket.


## Repairing a broken git/SSH configuration

This mode will re-create your .gitconfig file, fixing the unescaped spaces
contained in your home directory's name.  It will not re-generate your SSH key
so you won't have to reconfigure Bitbucket.

1. Open the Anaconda Command Line

2. Move into the new conda_fix directory created by the previous command:
   `cd conda_fix`

3. Run the conda_fix.py Python program with the argument 'repair':
   `python conda_fix.py repair`
