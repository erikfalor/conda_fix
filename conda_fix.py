import platform
import os
import os.path
import sys
import subprocess

SKIP_CONDA_INSTALL = False
DEBUGGING = False
RECONFIGURE = False
REPAIR_MODE = False

if 'repair' in sys.argv:
    SKIP_CONDA_INSTALL = True
    RECONFIGURE = True
    REGEN_SSH_KEY = False
    REPAIR_MODE = True
else:
    SKIP_CONDA_INSTALL = '-n' in sys.argv
    RECONFIGURE = '-r' in sys.argv
    REGEN_SSH_KEY = '-s' in sys.argv



# Sanity checks
if os.environ.get('CONDA_DEFAULT_ENV') == None:
    print('You must run this program from within the Anaconda environment')
    os.exit(1)
if platform.system() != 'Windows':
    print('You must run this program on Windows')
    os.exit(1)


if not SKIP_CONDA_INSTALL:
    print("Installing advanced OpenSSH and git packages with Conda")
    subprocess.run(['conda', 'install', '-y', 'git', 'm2-openssh'])
else:
    print("Skipping installation of git...")

sshDir = os.environ['USERPROFILE'] + "\\.ssh"
if not os.path.exists(sshDir):
    print("Creating SSH client configuration directory\n")
    os.makedirs(sshDir)


print("Creating SSH client configuration file")
ukhf = 'UserKnownHostsFile "' + os.environ['USERPROFILE'] + '\\.ssh\\known_hosts"'
ukhf = ukhf.replace('\\', '\\\\')
with open(sshDir + '\\config', 'w') as sshCfg: 
    sshCfg.write('# SSH client configuration file')
    sshCfg.write('\n')
    sshCfg.write(ukhf)


# Specify the directory containing commands to ensure that this script is running the correct commands
cmdPrefix = f'{os.environ["CONDA_PREFIX"]}\\Library'



print("Configuring git")
sshCommand = f"{cmdPrefix}\\usr\\bin\\ssh -i {os.environ['USERPROFILE']}\\.ssh\\id_rsa -F {os.environ['USERPROFILE']}\\.ssh\\config"
sshCommand = sshCommand.replace('\\', '\\\\')
subprocess.run([f'{cmdPrefix}\\bin\\git', 'config', '--global', 'core.sshCommand',   f'{sshCommand}'])
subprocess.run([f'{cmdPrefix}\\bin\\git', 'config', '--global', 'color.branch',      'always'])
subprocess.run([f'{cmdPrefix}\\bin\\git', 'config', '--global', 'color.diff',        'always'])
subprocess.run([f'{cmdPrefix}\\bin\\git', 'config', '--global', 'color.interactive', 'always'])
subprocess.run([f'{cmdPrefix}\\bin\\git', 'config', '--global', 'color.status',      'always'])
subprocess.run([f'{cmdPrefix}\\bin\\git', 'config', '--global', 'color.ui',          'always'])
subprocess.run([f'{cmdPrefix}\\bin\\git', 'config', '--global', 'pager.branch',      'false'])
subprocess.run([f'{cmdPrefix}\\bin\\git', 'config', '--global', 'core.editor',       'wordpad'])


print("Generating RSA public/private keypair")
subprocess.run([f'{cmdPrefix}\\usr\\bin\\ssh-keygen',  '-a', '100', '-P', '', '-f', f'{os.environ["USERPROFILE"]}\.ssh\id_rsa'])


if REPAIR_MODE:
    sshConfig = True
elif not RECONFIGURE and os.path.isdir(sshDir):
    yesNo = input("SSH directory detected.  Re-configure SSH? [Y/n] ")
    sshConfig = yesNo.lower().startswith('y') or yesNo == ''
else:
    sshConfig = True


if sshConfig:
    print("Creating SSH client configuration file\n")
    ukhf = 'UserKnownHostsFile "' + os.environ['USERPROFILE'] + '\\.ssh\\known_hosts"'
    ukhf = ukhf.replace('\\', '\\\\')
    with open(sshDir + '\\config', 'w') as sshCfg: 
        sshCfg.write('# SSH client configuration file')
        sshCfg.write('\n')
        sshCfg.write(ukhf)


if REPAIR_MODE:
    gitConfig = True
    if os.path.isfile(f"{os.environ['USERPROFILE']}\\.gitconfig"):
        os.remove(f"{os.environ['USERPROFILE']}\\.gitconfig")
elif not RECONFIGURE and os.path.isfile(f"{os.environ['USERPROFILE']}\\.gitconfig"):
    yesNo = input(".gitconfig detected.  Re-configure git? [Y/n] ")
    gitConfig = yesNo.lower().startswith('y') or yesNo == ''
else:
    gitConfig = True


def cmdEscape(s):
    return s.replace('\\', '\\\\').replace(' ', '\\ ')

if gitConfig:
    # Specify the directory containing commands to ensure that this script is running the correct commands
    cmdPrefix = f'{os.environ["CONDA_PREFIX"]}\\Library'

    print("Configuring git\n")
    userprofile = cmdEscape(os.environ['USERPROFILE'])
    sshCommand = ' '.join( map(cmdEscape, [
        f"{cmdPrefix}\\usr\\bin\\ssh",
        "-i", f"{os.environ['USERPROFILE']}\\.ssh\\id_rsa",
        "-F", f"{os.environ['USERPROFILE']}\\.ssh\\config" ]))

    subprocess.run([f'{cmdPrefix}\\bin\\git', 'config', '--global', 'core.sshCommand',   f'{sshCommand}'])
    subprocess.run([f'{cmdPrefix}\\bin\\git', 'config', '--global', 'color.branch',      'always'])
    subprocess.run([f'{cmdPrefix}\\bin\\git', 'config', '--global', 'color.diff',        'always'])
    subprocess.run([f'{cmdPrefix}\\bin\\git', 'config', '--global', 'color.interactive', 'always'])
    subprocess.run([f'{cmdPrefix}\\bin\\git', 'config', '--global', 'color.status',      'always'])
    subprocess.run([f'{cmdPrefix}\\bin\\git', 'config', '--global', 'color.ui',          'always'])
    subprocess.run([f'{cmdPrefix}\\bin\\git', 'config', '--global', 'pager.branch',      'false'])
    subprocess.run([f'{cmdPrefix}\\bin\\git', 'config', '--global', 'core.editor',       'wordpad'])


if REPAIR_MODE:
    sshKeyGen = False
elif not REGEN_SSH_KEY and os.path.isfile(f'{os.environ["USERPROFILE"]}\.ssh\id_rsa'):
    yesNo = input("SSH key detected.  Re-generate SSH key? [y/N] ")
    sshKeyGen = not(yesNo.lower().startswith('n') or yesNo == '')
else:
    sshKeyGen = True

if sshKeyGen:
    print("Generating RSA public/private keypair...\n")
    subprocess.run([f'{cmdPrefix}\\usr\\bin\\ssh-keygen',  '-a', '100', '-P', '', '-f', f'{os.environ["USERPROFILE"]}\.ssh\id_rsa'])
    print("""\n\n\n
=====================================================
Add your SSH key to Bitbucket for passwordless access
=====================================================

1. Log into your Bitbucket Account
2. Click on your Avatar -> Bitbucket settings -> Security -> SSH keys
3. Click on "Add key"
4. Copy and paste the entire contents of the Notepad window into Bitbucket
5. Click on "Add key" to save
6. Close the Notepad window when done""")

    os.system('notepad %USERPROFILE%\.ssh\id_rsa.pub')


# vim: set tabstop=4 expandtab:
